import 'package:flutter/material.dart';
import 'package:fzoom_example/screens/home.dart';
import 'package:fzoom_example/screens/join_meeting.dart';
import 'package:fzoom_example/screens/start_meeting.dart';
import 'package:fzoom_example/screens/start_non_login_meeting.dart';

void main() => runApp(ExampleApp());

class ExampleApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fzoom plugin Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      navigatorObservers: [],
      initialRoute: '/',
      routes: {
        '/': (context) => HomeWidget(),
        '/JoinMeeting': (context) => JoinMeetingWidget(),
        '/StartMeeting': (context) => StartMeetingWidget(),
        '/StartNonloginMeeting': (context) => StartNonloginMeetingWidget(),
      },
    );
  }
}
