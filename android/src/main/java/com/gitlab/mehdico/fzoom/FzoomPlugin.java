package com.gitlab.mehdico.fzoom;

import android.content.Context;
import android.widget.Toast;
import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import us.zoom.androidlib.util.StringUtil;
import us.zoom.sdk.ZoomApiError;
import us.zoom.sdk.ZoomError;
import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKInitParams;
import us.zoom.sdk.ZoomSDKInitializeListener;

/** FzoomPlugin */
public class FzoomPlugin implements FlutterPlugin, MethodCallHandler {

   private MethodChannel methodChannel;
   private Context applicationContext;

   @Override
   public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
      this.applicationContext = flutterPluginBinding.getApplicationContext();
      methodChannel           = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "zoom_methods");
      methodChannel.setMethodCallHandler(this);

      flutterPluginBinding.getPlatformViewRegistry()
                          .registerViewFactory("zoom_view", new ZoomViewFactory(flutterPluginBinding.getBinaryMessenger()));
   }

   @Override
   public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
      switch (call.method) {
         case "init":
            init(call, result);
            break;
         case "loginStatus":
            loginStatus(result);
            break;
         case "login":
            login(call, result);
            break;
         default:
            Toast.makeText(applicationContext, "notImplemented", Toast.LENGTH_SHORT).show();
            result.notImplemented();
      }
   }

   @Override
   public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
      applicationContext = null;
      methodChannel.setMethodCallHandler(null);
      methodChannel = null;
   }

   private void init(final MethodCall methodCall, final MethodChannel.Result result) {

      Map<String, String> options = methodCall.arguments();

      ZoomSDK zoomSDK = ZoomSDK.getInstance();

      if (zoomSDK.isInitialized()) {
         List<Integer> response = Arrays.asList(0, 0);
         result.success(response);
         return;
      }

      ZoomSDKInitParams initParams = new ZoomSDKInitParams();
      initParams.appKey    = options.get("appKey");
      initParams.appSecret = options.get("appSecret");
      initParams.domain    = options.get("domain");

      zoomSDK.initialize(applicationContext, new ZoomSDKInitializeListener() {

                            @Override
                            public void onZoomAuthIdentityExpired() {

                            }

                            @Override
                            public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {
                               List<Integer> response = Arrays.asList(errorCode, internalErrorCode);

                               if (errorCode != ZoomError.ZOOM_ERROR_SUCCESS) {
                                  System.out.println("Failed to initialize Zoom SDK");
                                  result.success(response);
                                  return;
                               }
                               //
                               //ZoomSDK zoomSDK = ZoomSDK.getInstance();
                               //MeetingService meetingService = zoomSDK.getMeetingService();
                               //meetingStatusChannel.setStreamHandler(new StatusStreamHandler(meetingService));
                               result.success(response);
                            }
                         },
                         initParams);
   }

   private void loginStatus(MethodChannel.Result result) {

      ZoomSDK zoomSDK = ZoomSDK.getInstance();

      if (!zoomSDK.isInitialized()) {
         System.out.println("Not initialized!!!!!!");
         result.success(Arrays.asList("LOGIN_STATUS_UNKNOWN", "SDK not initialized"));
         return;
      }

      result.success(zoomSDK.isLoggedIn()
                     ? Arrays.asList("LOGIN_NO_ERROR", "")
                     : Arrays.asList("LOGIN_ERROR", ""));
   }

   private void login(MethodCall methodCall, MethodChannel.Result result) {
      Map<String, String> options = methodCall.arguments();

      ZoomSDK zoomSDK = ZoomSDK.getInstance();

      if (!zoomSDK.isInitialized()) {
         System.out.println("Not initialized!!!!!!");
         result.success(false);
         return;
      }

      String userName = options.get("email");
      String password = options.get("password");
      if (StringUtil.isEmptyOrNull(userName) || StringUtil.isEmptyOrNull(password)) {
         Toast.makeText(applicationContext, "You need to enter user name and password.", Toast.LENGTH_LONG).show();
         return;
      }

      if (!(zoomSDK.loginWithZoom(userName, password) == ZoomApiError.ZOOM_API_ERROR_SUCCESS)) {
         System.out.println("ZoomSDK has not been initialized successfully or sdk is logging in.");
         result.success(false);
      } else {
         System.out.println("login Successful.");
         result.success(true);
      }
   }
}
