package com.gitlab.mehdico.fzoom.helpers;

public interface OnTaskCompleted {
   void onRetrieveUserInfoTaskCompleted();
}