#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint fzoom.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'fzoom'
  s.version          = '0.0.1'
  s.summary          = 'Flutter Zoom Library'
  s.description      = <<-DESC
Flutter Zoom Plugin
                       DESC
  s.homepage         = 'https://gitlab.com/mehdico/fzoom'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Mehdi Mousavi' => 'malemir@gmail.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
#  s.platform = :ios, '8.0'

  s.ios.deployment_target = '8.0'


  s.xcconfig = { 'OTHER_LDFLAGS' => '-framework MobileRTC' }
  s.preserve_paths = 'MobileRTC.framework', 'MobileRTCResources.bundle'
  s.vendored_frameworks = 'MobileRTC.framework'
  s.resource = 'MobileRTCResources.bundle'
  
  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.swift_version = '5.0'
  
end
