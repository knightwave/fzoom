class ZoomResult {
  static const ERROR_INIT_FAILED = -1;
  static const ERROR_SERVICE_NULL = -2;
  static const ERROR_USER_INFO_NULL = -3;
  static const ERROR_LOGIN = -4;
  static const ERROR_LOGOUT = -5;
  static const ERROR_MEETING_NOT_FOUND = -6;
  static const ERROR_MEETING_START = -7;
  static const ERROR_MEETING_JOIN = -8;
  static const SUCCESS = 0;
}
