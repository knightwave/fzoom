import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fzoom/zoom_options.dart';
import 'package:fzoom/zoom_result.dart';

typedef void ZoomViewCreatedCallback(ZoomViewController controller);

class ZoomView extends StatefulWidget {
  const ZoomView({
    Key key,
    this.onViewCreated,
  }) : super(key: key);

  final ZoomViewCreatedCallback onViewCreated;

  @override
  State<StatefulWidget> createState() => _ZoomViewState();
}

class _ZoomViewState extends State<ZoomView> {
  @override
  Widget build(BuildContext context) {
    if (defaultTargetPlatform == TargetPlatform.android) {
      return AndroidView(
        viewType: 'zoom_view',
        onPlatformViewCreated: _onPlatformViewCreated,
      );
    }
    if (defaultTargetPlatform == TargetPlatform.iOS) {
      return UiKitView(
        viewType: 'zoom_view',
        onPlatformViewCreated: _onPlatformViewCreated,
      );
    }
    return Text('$defaultTargetPlatform is not yet supported');
  }

  void _onPlatformViewCreated(int id) {
    if (widget.onViewCreated == null) {
      return;
    }

    var controller = new ZoomViewController._(id);
    widget.onViewCreated(controller);
  }
}

class ZoomViewController {
  ZoomViewController._(int id)
      : _methodChannel = new MethodChannel('zoom_view'),
        _zoomStatusEventChannel = new EventChannel("zoom_event_stream_" + id.toString());

  final MethodChannel _methodChannel;
  final EventChannel _zoomStatusEventChannel;

  Future<int> joinMeetingNoLogin(ZoomJoinMeetingParams params) async {
    assert(params != null);

    var optionMap = new Map<String, String>();
    optionMap.putIfAbsent("appKey", () => params.appKey);
    optionMap.putIfAbsent("appSecret", () => params.appSecret);
    optionMap.putIfAbsent("domain", () => params.domain);
    optionMap.putIfAbsent("displayName", () => params.displayName);
    optionMap.putIfAbsent("meetingId",
        () => (params.meetingId.isEmpty == null || params.meetingId.isEmpty ? "" : params.meetingId.trim()));
    optionMap.putIfAbsent("meetingPassword", () => params.meetingPassword);
    optionMap.putIfAbsent("disableDialIn", () => params.disableDialIn);
    optionMap.putIfAbsent("disableDrive", () => params.disableDrive);
    optionMap.putIfAbsent("disableInvite", () => params.disableInvite);
    optionMap.putIfAbsent("disableShare", () => params.disableShare);
    optionMap.putIfAbsent("noDisconnectAudio", () => params.noDisconnectAudio);
    optionMap.putIfAbsent("noAudio", () => params.noAudio);
    var initResult = await _methodChannel.invokeMethod('init', optionMap);
    if (initResult[0] != 0) {
      return ZoomResult.ERROR_INIT_FAILED;
    }

    var logoutResult = await _methodChannel.invokeMethod('logout');
    if (logoutResult != 0) {
      debugPrint("logout error");
      return ZoomResult.ERROR_LOGOUT;
    }

    var joinResult = await _methodChannel.invokeMethod('joinMeetingNoLogin', optionMap);
    return joinResult;
  }

  Future<int> startMeeting(ZoomStartMeetingParams params) async {
    assert(params != null);

    var optionMap = new Map<String, String>();
    optionMap.putIfAbsent("appKey", () => params.appKey);
    optionMap.putIfAbsent("appSecret", () => params.appSecret);
    optionMap.putIfAbsent("domain", () => params.domain);
    optionMap.putIfAbsent("userId", () => params.userId);
    optionMap.putIfAbsent("password", () => params.password);
    optionMap.putIfAbsent("disableDialIn", () => params.disableDialIn);
    optionMap.putIfAbsent("disableDrive", () => params.disableDrive);
    optionMap.putIfAbsent("disableInvite", () => params.disableInvite);
    optionMap.putIfAbsent("disableShare", () => params.disableShare);
    optionMap.putIfAbsent("noDisconnectAudio", () => params.noDisconnectAudio);
    optionMap.putIfAbsent("noVideo", () => params.noVideo);

    var initResult = await _methodChannel.invokeMethod('init', optionMap);
    if (initResult[0] != 0) {
      debugPrint("init error");
      return ZoomResult.ERROR_INIT_FAILED;
    }
    debugPrint("init success");

    var logoutResult = await _methodChannel.invokeMethod('logout');
    if (logoutResult != 0) {
      debugPrint("logout error");
      return ZoomResult.ERROR_LOGOUT;
    }

    var loginOptions = new Map<String, String>();
    loginOptions.putIfAbsent("userId", () => params.userId);
    loginOptions.putIfAbsent("password", () => params.password);
    var loginResult = await _methodChannel.invokeMethod('login', loginOptions);
    if (loginResult != 0) {
      debugPrint("login error");
      return ZoomResult.ERROR_LOGIN;
    }
    debugPrint("login success");

    var startResult = await _methodChannel.invokeMethod('startMeeting', optionMap);
    return startResult;
  }

  Future<List> getCurrentMeetingInviteInfo() async {
    var meetingInfoResult = await _methodChannel.invokeMethod('currentMeetingInviteInfo');
    return meetingInfoResult;
  }

  Future<int> startMeetingNoLogin(ZoomStartMeetingNoLoginParams params) async {
    assert(params != null);

    var optionMap = new Map<String, String>();
    optionMap.putIfAbsent("appKey", () => params.appKey);
    optionMap.putIfAbsent("appSecret", () => params.appSecret);
    optionMap.putIfAbsent("apiKey", () => params.apiKey);
    optionMap.putIfAbsent("apiSecret", () => params.apiSecret);
    optionMap.putIfAbsent("domain", () => params.domain);
    optionMap.putIfAbsent("userId", () => params.userId);
    optionMap.putIfAbsent("meetingId",
        () => (params.meetingId.isEmpty == null || params.meetingId.isEmpty ? "" : params.meetingId.trim()));
    optionMap.putIfAbsent("displayName", () => params.displayName);
    optionMap.putIfAbsent("disableInvite", () => params.disableInvite);
    optionMap.putIfAbsent("disableShare", () => params.disableShare);
    optionMap.putIfAbsent("disableDrive", () => params.disableDrive);
    optionMap.putIfAbsent("disableDialIn", () => params.disableDialIn);
    optionMap.putIfAbsent("noDisconnectAudio", () => params.noDisconnectAudio);
    optionMap.putIfAbsent("noVideo", () => params.noVideo);

    var initResult = await _methodChannel.invokeMethod('init', optionMap);
    if (initResult[0] != 0) {
      debugPrint("init error");
      return ZoomResult.ERROR_INIT_FAILED;
    }
    debugPrint("init success");

    var logoutResult = await _methodChannel.invokeMethod('logout');
    if (logoutResult == false) {
      debugPrint("logout error");
      return ZoomResult.ERROR_LOGOUT;
    }

    var startResult = await _methodChannel.invokeMethod('startMeetingNoLogin', optionMap);
    debugPrint(startResult == 0 ? "startMeetingNoLogin success" : "startMeetingNoLogin error");
    return startResult;
  }

  Future<List> meetingStatus() async {
    return _methodChannel.invokeMethod('meetingStatus');
  }

  Stream<dynamic> get zoomStatusEvents {
    return _zoomStatusEventChannel.receiveBroadcastStream();
  }
}
